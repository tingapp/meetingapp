package com.meetingapp.macbook.meetingapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.models.ChatMessage;
import com.meetingapp.macbook.meetingapp.network.ChattingNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

/**
 * Created by macbook on 16. 8. 31..
 */
public class ChattingActivity extends AppCompatActivity{
    public static final String CHATTING_ID = "teamId";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String chattingId = getIntent().getStringExtra(CHATTING_ID);

        setContentView(R.layout.activity_chatting);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.activity_chatting_recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);


        final EditText editText = (EditText) findViewById(R.id.activity_chatting_edittext);
        final Button sendButton = (Button) findViewById(R.id.activity_chatting_send_button);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editText.getText().toString().isEmpty()) {
                    sendButton.setEnabled(true);
                } else {
                    sendButton.setEnabled(false);
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatMessage chatMessage = new ChatMessage(editText.getText().toString(), UserLab.getInstance().getUserId());
                ChattingNetwork.sendMessage(chattingId, chatMessage);
                editText.setText("");
            }
        });


    }
}
