package com.meetingapp.macbook.meetingapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.fragments.ProfileFragment;
import com.meetingapp.macbook.meetingapp.network.StorageNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;
import com.meetingapp.macbook.meetingapp.utils.ProfileImageDialogFragment;

/**
 * Created by JiwoonWon on 16. 8. 24..
 */
public class ProfileEditActivity extends AppCompatActivity {
    private static final String TAG = ProfileEditActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        final String userId = UserLab.getInstance().getUserId();
        Log.d(TAG, "userId:" + userId);

        final EditText usernameEditText = (EditText) findViewById(R.id.activity_profile_edit_name);
        final EditText userNicknameEditText = (EditText) findViewById(R.id.activity_profile_edit_nickname);
        final EditText ageEditText = (EditText) findViewById(R.id.activity_profile_edit_age);
        final EditText universityEditText = (EditText) findViewById(R.id.activity_profile_edit_university);

        String profileNickname = getIntent().getStringExtra(ProfileFragment.PROFILE_NICKNAME);
        String profileUsername = getIntent().getStringExtra(ProfileFragment.PROFILE_USERNAME);
        String profileAge = getIntent().getStringExtra(ProfileFragment.PROFILE_AGE);
        String profileUniversity = getIntent().getStringExtra(ProfileFragment.PROFILE_UNIVERSITY);

        usernameEditText.setText(profileUsername);
        userNicknameEditText.setText(profileNickname);
        ageEditText.setText(profileAge);
        universityEditText.setText(profileUniversity);

        ImageView backButtonImageView = (ImageView) findViewById(R.id.activity_profile_edit_back);
        final ImageView profileImageView1 = (ImageView) findViewById(R.id.activity_profile_edit_profile01);
        TextView saveButtonTextView = (TextView) findViewById(R.id.activity_profile_edit_save);

        new ProfileImageDialogFragment.setSelfImage(ProfileEditActivity.this, new ImageView[] {
                profileImageView1,

        });

        backButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        saveButtonTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameEditText.getText().toString();
                String nickname = userNicknameEditText.getText().toString();
                String age = ageEditText.getText().toString();
                String university = universityEditText.getText().toString();

                //Firebase에 업로드 하는 코드 구현
                UserNetwork.setUserToDatabase(userId, nickname, username, age, university);

                StorageNetwork.uploadProfileImage(UserLab.getInstance().getUserId(), "profile1", profileImageView1);

                //네트워크에 제대로 된 데이터가 저장되었는지 확인하는 작업이 필요
                //업데이트 된 내용이 프로필에 반영되게
                finish();
            }
        });
    }
}