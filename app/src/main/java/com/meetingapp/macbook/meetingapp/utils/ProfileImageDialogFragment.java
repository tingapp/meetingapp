package com.meetingapp.macbook.meetingapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.meetingapp.macbook.meetingapp.R;

/**
 * Created by macbook on 16. 9. 4..
 */
public class ProfileImageDialogFragment extends DialogFragment {
    private static final String TAG = ProfileImageDialogFragment.class.getSimpleName();
    private static final int REQUEST_GET_IMAGE = 200;
    private ImageView mProfileImageView;

    public ProfileImageDialogFragment setImageView(ImageView imageView) {
        mProfileImageView = imageView;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.item_dialog_select_profile, null);

        View editProfile = view.findViewById(R.id.item_dialog_select_profile_add);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Dialog:choice:edit");
                Intent intent = new Intent(Intent.ACTION_PICK, Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, REQUEST_GET_IMAGE);
            }
        });

/*        View defaultProfile = view.findViewById(R.id.item_dialog_select_profile_default
        );
        defaultProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Dialog:choice:null");
                //기본 이미지로 변경
                Bitmap photo = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_profile_dummy);
                mProfileImageView.setImageBitmap(photo);

                ProfileImageDialogFragment.this.dismiss();
            }
        });*/

        View cancelDialog = view.findViewById(R.id.item_dialog_select_profile_cancel);
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileImageDialogFragment.this.dismiss();
            }
        });


        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult");
        if (requestCode == REQUEST_GET_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            Uri imageUri = data.getData();
            String[] filePathColumn = { Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(imageUri,filePathColumn, null, null, null);
            cursor.moveToFirst();

            String picturePath = cursor.getString(cursor.getColumnIndex(Media.DATA));
            Log.d(TAG, "onActivityResult:picturePath:" + picturePath);
            cursor.close();

            mProfileImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            ProfileImageDialogFragment.this.dismiss();
        }
    }

    public static class setSelfImage {
        public setSelfImage(final FragmentActivity activity, final ImageView[] imageViewList) {
            for (final ImageView imageView : imageViewList){
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProfileImageDialogFragment profileImageDialogFragment = new ProfileImageDialogFragment();
                            profileImageDialogFragment
                                    .setImageView(imageView)
                                    .show(activity.getSupportFragmentManager(), "ProfileImageDialogFragment");
                    }
                });
            }
        }
    }
}
