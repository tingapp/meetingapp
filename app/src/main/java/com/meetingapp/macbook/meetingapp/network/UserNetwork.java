package com.meetingapp.macbook.meetingapp.network;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.models.User;

import java.io.IOException;

/**
 * Created by macbook on 16. 8. 16..
 */
public class UserNetwork extends DatabaseNetwork {
    public static final String FIREBASE_USERS = "users";
    public static final String FIREBASE_USERNAME = "username";
    public static final String FIREBASE_NICKNAME = "nickname";
    public static final String FIREBASE_AGE = "age";
    public static final String FIREBASE_UNIVERSITY = "university";
    public static final String FIREBASE_TEAMIDS = "teamIds";
    public static final String FIREBASE_LIKETEAMS = "likeTeams";

    public static void setUserToDatabase(String userId, String nickname, String username, String age, String university) {
        sDatabase.child(FIREBASE_USERS).child(userId).child(FIREBASE_NICKNAME).setValue(nickname);
        sDatabase.child(FIREBASE_USERS).child(userId).child(FIREBASE_USERNAME).setValue(username);
        sDatabase.child(FIREBASE_USERS).child(userId).child(FIREBASE_AGE).setValue(age);
        sDatabase.child(FIREBASE_USERS).child(userId).child(FIREBASE_UNIVERSITY).setValue(university);
        Log.d("UserNetwork", "setUserToDatabase: " + "userId:" + userId + " nickname" + nickname + " username:" + username + " age:" + age + " university:" + university);
    }

    public abstract static class GetUserHandler {
        public GetUserHandler() {}
        private static final String TAG = GetUserHandler.class.getSimpleName();

        public abstract void doWithUserSnapshot(DataSnapshot userSnapshot);

        public void getUserById(String userId) {
            SnapshotListener snapshotListener = new SnapshotListener() {
                @Override
                public void doWithSnapshot(DataSnapshot userSnapshot) {
                    doWithUserSnapshot(userSnapshot);
                }
            };
            sDatabase.child(FIREBASE_USERS)
                    .child(userId)
                    .addListenerForSingleValueEvent(snapshotListener);
        }
    }

    public abstract static class FindUserHandler extends Exception{
        public static final int CLUETYPE_ID = 0;
        public static final int CLUETYPE_NICKNAME = 1;
        public FindUserHandler() {}
        private static final String TAG = FindUserHandler.class.getSimpleName();

        public abstract void doWithUserSnapshot(DataSnapshot userSnapshot);
        public abstract void doWhenfriendIsNotFound();

        public void findUser(final int clueType, final String clue) {
            if (clueType != CLUETYPE_ID && clueType != CLUETYPE_NICKNAME) {throw new RuntimeException();}

            SnapshotListener snapshotListener = new SnapshotListener() {
                @Override
                public void doWithSnapshot(DataSnapshot dataSnapshot) {
                    Boolean friendIsFound = false;

                    for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                        String foundClue;
                        switch (clueType) {
                            case CLUETYPE_ID : foundClue = userSnapshot.getKey();
                                Log.d(TAG, "findUser: case Id foundClue:" + foundClue);
                                break;
                            default: foundClue = userSnapshot.child(FIREBASE_NICKNAME).getValue(String.class);
                                Log.d(TAG, "findUser: case nickname foundClue:" + foundClue);
                                break;
                        }
                        Log.d(TAG, "findUser: foundClue:" + foundClue);
                        //User Data에 하나씩 접속, 입력한 Id or nickname과 일치하는 clue 검색
                        if (foundClue.equals(clue)) {
                            doWithUserSnapshot(userSnapshot);

                            friendIsFound = true;
                            break;
                        }
                    }
                    if (friendIsFound == false) {
                        doWhenfriendIsNotFound();
                    }
                }
            };
            sDatabase.child(FIREBASE_USERS)
                    .addListenerForSingleValueEvent(snapshotListener);
        }
    }

    public static class GetTeamsHandler {
        public static final String STATUS_UNMATCHED = TeamNetwork.FIREBASE_STATUS_UNMATCHED;
        public static final String STATUS_MATCHED = "matched";

        public GetTeamsHandler() {}
        private static final String TAG = GetTeamsHandler.class.getSimpleName();

        public void doWithTeamSnapshot(DataSnapshot teamSnapshot) {}

        public void getTeams(final String teamStatusOption, String userId) {
            SnapshotListener teamListListener = new SnapshotListener() {
                @Override
                public void doWithSnapshot(DataSnapshot dataSnapshot) {
                    for (DataSnapshot teamIdSnapshot : dataSnapshot.getChildren()) {
                        String teamId = teamIdSnapshot.getValue(String.class);
                        Log.d(TAG, "getTeams: teamId:" + teamId);

                        SnapshotListener teamStatusListener = new SnapshotListener() {
                            @Override
                            public void doWithSnapshot(DataSnapshot teamSnapshot) {
                                String teamStatus = teamSnapshot.child(TeamNetwork.FIREBASE_STATUS).getValue(String.class);
                                Log.d(TAG, "getTeams:teamStatus:" + teamStatus);
                                if (teamStatus != null) {
                                    switch (teamStatusOption) {
                                        case STATUS_UNMATCHED:
                                            Log.d(TAG, "getTeams:case_unmatched");
                                            if (teamStatus.equals(teamStatusOption)) {
                                                getFilteredTeam(teamSnapshot);
                                                Log.d(TAG, "getTeams:getUnmatchedFilteredTeam");
                                            }
                                            break;
                                        default:
                                            Log.d(TAG, "getTeams:case_matched");
                                            if (!teamStatus.equals(TeamNetwork.FIREBASE_STATUS_DESTROYED)
                                                    && !teamStatus.equals(TeamNetwork.FIREBASE_STATUS_UNMATCHED)) {
                                                getFilteredTeam(teamSnapshot);
                                                Log.d(TAG, "getTeams:getMatchedFilteredTeam");
                                            }
                                            break;
                                    }
                                }
                            }
                            private void getFilteredTeam(DataSnapshot teamSnapshot) {
                                doWithTeamSnapshot(teamSnapshot);
                                Log.d(TAG, "getTeams:filteredTeamId:" + teamSnapshot.getKey());
                            }
                        };
                        sDatabase.child(TeamNetwork.FIREBASE_TEAMS)
                                .child(teamId)
                                .addListenerForSingleValueEvent(teamStatusListener);
                        }
                    }
                };
            sDatabase.child(FIREBASE_USERS)
                    .child(userId)
                    .child(FIREBASE_TEAMIDS)
                    .addListenerForSingleValueEvent(teamListListener);
        }
    }
}
