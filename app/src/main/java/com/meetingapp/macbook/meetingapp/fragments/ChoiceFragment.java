package com.meetingapp.macbook.meetingapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.models.User;
import com.meetingapp.macbook.meetingapp.network.TeamNetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by macbook on 16. 8. 15..
 */
public class ChoiceFragment extends Fragment {
    private static final String TAG = ChoiceFragment.class.getSimpleName();
    private List<TextView> mTextViewList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choice, container, false);
        Log.d(TAG, "onCreateView");

        TextView member1View = (TextView) view.findViewById(R.id.fragment_choice_member1);
        TextView member2View = (TextView) view.findViewById(R.id.fragment_choice_member2);
        TextView member3View = (TextView) view.findViewById(R.id.fragment_choice_member3);
        TextView member4View = (TextView) view.findViewById(R.id.fragment_choice_member4);
        mTextViewList = Arrays.asList(member1View, member2View, member3View, member4View);

        final ShowNewTeamHandler showNewTeamHandler = new ShowNewTeamHandler();
        showNewTeamHandler.getNewTeam();

        Button likeButton = (Button) view.findViewById(R.id.fragment_choice_like_button);
        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeamNetwork.setLikeTeam(showNewTeamHandler.getTeamId());
                showNewTeamHandler.getNewTeam();
            }
        });

        Button passButton = (Button) view.findViewById(R.id.fragment_choice_pass_button);
        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewTeamHandler.getNewTeam();
            }
        });

        return view;
    }

    private class ShowNewTeamHandler extends TeamNetwork.GetNewTeamHandler {

        public ShowNewTeamHandler() {
            super();
        }

        public String getTeamId() {
            return super.mTeamId;
        }

        @Override
        public void doWithUserSnapshot(int index, DataSnapshot userSnapshot) {
            String username = userSnapshot.getValue(User.class).getUsername();
            mTextViewList.get(index).setText(username);
        }

    }
}
