package com.meetingapp.macbook.meetingapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.network.StorageNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.RegisterActivityLab;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;
import com.meetingapp.macbook.meetingapp.utils.ProfileImageDialogFragment;

/**
 * Created by macbook on 16. 8. 13..
 */
public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private static final int REQUEST_GET_IMAGE = 0;
    public static final String IMAGEVIEWID = "imageViewId";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mFirebaseUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        Log.d(TAG, "accessToken:" + accessToken);
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mFirebaseUser = mAuth.getCurrentUser();

                if (mFirebaseUser != null) {
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + mFirebaseUser.getUid());

                    final String existingUserId = mFirebaseUser.getUid();

                    UserNetwork.sDatabase.child(UserNetwork.FIREBASE_USERS).addListenerForSingleValueEvent(
                            new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            boolean userExists = false;

                            for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                                String userId = userSnapshot.getKey();
                                Log.d(TAG, "onDataChange:userId:" + userId);
                                if (userId.equals(existingUserId)) {
                                    Log.d(TAG, "Existing User is Found");
                                    userExists = true;
                                    UserLab.getInstance(existingUserId);
                                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }

                            if (userExists == false) {
                                Log.d(TAG, "Existing User is not Found");
                                updateUI();
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        handleFacebookAccessToken(accessToken);
    }

    private void updateUI() {
        setContentView(R.layout.activity_register);

        final EditText mUserNicknameEditText = (EditText) findViewById(R.id.activity_register_nickname);
        final EditText mUsernameEditText = (EditText) findViewById(R.id.activity_register_username);
        final EditText mAgeEditText = (EditText) findViewById(R.id.activity_register_age);
        final EditText mUniversityEditText = (EditText) findViewById(R.id.activity_register_university);

        final ImageView profileImageView = (ImageView) findViewById(R.id.activity_register_profile);
        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //클릭했을 때 사진을 가져오도록
                new ProfileImageDialogFragment.setSelfImage(RegisterActivity.this, new ImageView[] {profileImageView});

            Log.d(TAG, "Dialog Created");
            }
        });

        Button submitButton = (Button) findViewById(R.id.activity_register_submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = mFirebaseUser.getUid();
                String mNickname = mUserNicknameEditText.getText().toString();
                String mUsername = mUsernameEditText.getText().toString();
                String mAge = mAgeEditText.getText().toString();
                String mUniversity = mUniversityEditText.getText().toString();

                //validation is required here, nickname should not be duplicatable
                UserNetwork.setUserToDatabase(userId, mNickname, mUsername, mAge, mUniversity);

                // Get the data from an ImageView as bytes
                StorageNetwork.uploadProfileImage(userId, "profile1", profileImageView);

                UserLab.getInstance(userId);

                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        Log.d(TAG, "handleFacebookAccessToken");

        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());

        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                if (!task.isSuccessful()) {
                    Log.w(TAG, "signInWithCredential", task.getException());
                    Toast.makeText(RegisterActivity.this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                } else {

                    Log.d(TAG, "signInWithCredential Succeeded");
                }
            }
        });
    }
}
