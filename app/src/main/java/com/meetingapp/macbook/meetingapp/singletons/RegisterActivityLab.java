package com.meetingapp.macbook.meetingapp.singletons;

import com.meetingapp.macbook.meetingapp.activities.RegisterActivity;

/**
 * Created by macbook on 16. 9. 4..
 */
public class RegisterActivityLab {
    private static RegisterActivityLab sRegisterActivityLab;
    private final RegisterActivity mRegisterActivity;

    private RegisterActivityLab(RegisterActivity registerActivity) {
        mRegisterActivity = registerActivity;
    }

    public static RegisterActivityLab getInstance() {
        return sRegisterActivityLab;
    }

    public static RegisterActivityLab getInstance(RegisterActivity registerActivity) {
        if (sRegisterActivityLab == null) {
            sRegisterActivityLab = new RegisterActivityLab(registerActivity);
        }
        return sRegisterActivityLab;
    }

    public RegisterActivity getRegisterActivity() {
        return mRegisterActivity;
    }
}
