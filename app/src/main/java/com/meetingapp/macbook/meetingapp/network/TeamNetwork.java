package com.meetingapp.macbook.meetingapp.network;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.models.Team;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by macbook on 16. 8. 21..
 */
public class TeamNetwork extends DatabaseNetwork {
    private static final String TAG = TeamNetwork.class.getSimpleName();

    public static final String FIREBASE_TEAMS = "teams";
    public static final String FIREBASE_MEMBERIDS = "memberIds";
    public static final String FIREBASE_STATUS = "status";
    public static final String FIREBASE_STATUS_DESTROYED = "destroyed";
    public static final String FIREBASE_STATUS_UNMATCHED = "unmatched";
    private static final String FIREBASE_INTRODUCTION = "introduction";

    public static void setTeam(List<String> memberList, String introduction) {
        String teamId = sDatabase.child(FIREBASE_TEAMS).push().getKey();

        for (String friendId : memberList) {
            sDatabase.child(UserNetwork.FIREBASE_USERS)
                    .child(friendId)
                    .child(UserNetwork.FIREBASE_TEAMIDS)
                    .child(teamId)
                    .setValue(teamId);
        }
        sDatabase.child(FIREBASE_TEAMS).child(teamId).setValue(new Team(memberList, FIREBASE_STATUS_UNMATCHED, introduction));
    }

    public static void setLikeTeam(final String opposingTeamId) {
        SnapshotListener teamsListener = new SnapshotListener() {
            @Override
            public void doWithSnapshot(DataSnapshot dataSnapshot) {
                //내가 속한 팀의 아이디를 하나씩 찾아놓음
                for (DataSnapshot teamSnapshot : dataSnapshot.getChildren()) {
                    final String myTeamId = teamSnapshot.getValue(String.class);
                    Log.d(TAG, "setLikeTeam:myTeamId:" + myTeamId);

                    SnapshotListener teamListener = new SnapshotListener() {
                        @Override
                        public void doWithSnapshot(DataSnapshot dataSnapshot) {
                            //내가 좋아요를 누른 팀의 모든 멤버를 하나씩 찾음
                            for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {
                                String memberId = memberSnapshot.getValue(String.class);
                                Log.d(TAG, "setLikeTeam:memberId:" + memberId);

                                SnapshotListener memberListener = new SnapshotListener() {
                                    @Override
                                    public void doWithSnapshot(DataSnapshot dataSnapshot) {
                                        //찾은 각각의 멤버가 좋아한 팀 아이디가 내가 속한 팀 아이디와 일치하는지 확인
                                        for (DataSnapshot likeTeamSnapshot : dataSnapshot.getChildren()) {
                                            String likeTeamId = likeTeamSnapshot.getValue(String.class);
                                            Log.d(TAG, "setLikeTeam:likeTeamId:" + likeTeamId);
                                            if (likeTeamId.equals(myTeamId)) {
                                                Log.d(TAG, "setLikeTeam: Succefully Matched!!");
                                                //매칭 ㄱㄱ싱!!
                                                String chattingId = sDatabase.child(ChattingNetwork.FIREBASE_CHATTING)
                                                        .push().getKey();
                                                //채팅 데이터 생성
                                                sDatabase.child(ChattingNetwork.FIREBASE_CHATTING)
                                                        .child(chattingId)
                                                        .child(ChattingNetwork.FIREBASE_TEAMIDS)
                                                        .setValue(new ArrayList<>(Arrays.asList(myTeamId, opposingTeamId)));
                                                //상대팀 데이터에 우리팀 아이디를 저장
                                                sDatabase.child(FIREBASE_TEAMS)
                                                        .child(opposingTeamId)
                                                        .child(FIREBASE_STATUS)
                                                        .setValue(chattingId);
                                                //우리팀 데이터에 상대팀 아이디를 저장
                                                sDatabase.child(FIREBASE_TEAMS)
                                                        .child(myTeamId)
                                                        .child(FIREBASE_STATUS)
                                                        .setValue(chattingId);
                                            }
                                        }
                                    }
                                };
                                sDatabase.child(UserNetwork.FIREBASE_USERS)
                                        .child(memberId)
                                        .child(UserNetwork.FIREBASE_LIKETEAMS)
                                        .addListenerForSingleValueEvent(memberListener);
                            }
                        }
                    };
                    sDatabase.child(FIREBASE_TEAMS)
                            .child(opposingTeamId)
                            .child(FIREBASE_MEMBERIDS)
                            .addListenerForSingleValueEvent(teamListener);
                }
            }
        };
        sDatabase.child(UserNetwork.FIREBASE_USERS)
                .child(UserLab.getInstance().getUserId())
                .child(UserNetwork.FIREBASE_TEAMIDS)
                .addListenerForSingleValueEvent(teamsListener);

        Log.d(TAG, "setLikeTeam:set opposingTeamId" + opposingTeamId + " userId:" + UserLab.getInstance().getUserId());

        //로그인한 사용자의 유저 데이터베이스에 어느팀을 좋아했는지가 저장됨
        sDatabase.child(UserNetwork.FIREBASE_USERS)
                .child(UserLab.getInstance().getUserId())
                .child(UserNetwork.FIREBASE_LIKETEAMS)
                .child(opposingTeamId)
                .setValue(opposingTeamId);
    }

    public abstract static class GetTeamHandler {
        private static final String TAG = GetTeamHandler.class.getSimpleName();

        public GetTeamHandler() {
        }

        public abstract void doWithMemberSnapshot(DataSnapshot memberSnapshot);

        public void getMember(String teamId) {
            SnapshotListener snapshotListener = new SnapshotListener() {
                @Override
                public void doWithSnapshot(DataSnapshot dataSnapshot) {
                    for (DataSnapshot memberSnapshot : dataSnapshot.getChildren()) {
                        String memberId = memberSnapshot.getValue(String.class);
                        Log.d(TAG, "onDataChange:memberId:" + memberId);

                        new MemberHandler().getUserById(memberId);
                    }
                }
            };
            sDatabase.child(FIREBASE_TEAMS)
                    .child(teamId)
                    .child(FIREBASE_MEMBERIDS)
                    .addListenerForSingleValueEvent(snapshotListener);
        }

        public class MemberHandler extends UserNetwork.GetUserHandler {
            public MemberHandler() {
                super();
            }

            @Override
            public void doWithUserSnapshot(DataSnapshot userSnapshot) {
                GetTeamHandler.this.doWithMemberSnapshot(userSnapshot);
            }
        }
    }

    public static class GetNewTeamHandler {
        private static final String TAG = GetNewTeamHandler.class.getSimpleName();
        protected String mTeamId;

        public GetNewTeamHandler() {}

        public void doWithUserSnapshot(int index, DataSnapshot userSnapshot) {}

        public void getNewTeam() {
            SnapshotListener snapshotListener = new SnapshotListener() {
                @Override
                public void doWithSnapshot(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "getNewTeam:onDataChange childrenCount:" + dataSnapshot.getChildrenCount());

                    try {
                        //randomNumber 설정
                        int randomNumber = new Random().nextInt((int) dataSnapshot.getChildrenCount()) + 1;
                        Log.d(TAG, "getNewTeam:randomNumber:" + randomNumber);

                        //모든 팀 중 앞에서부터 하나씩 읽다가 randomNumber번째 팀을 읽을 때 멈춤
                        int count = 0;
                        for (DataSnapshot randomSnapshot : dataSnapshot.getChildren()) {
                            count += 1;
                            Log.d(TAG, "getNewTeam:count:" + count);
                            if (count == randomNumber) {
                                mTeamId = randomSnapshot.getKey();
                                Log.d(TAG, "getNewTeam:memberIds" + randomSnapshot.child(FIREBASE_MEMBERIDS).getValue());
                                List<String> list = (List<String>) randomSnapshot.child(FIREBASE_MEMBERIDS).getValue();

                                //팀 list 에서 멤버 아이디 하나씩 접속해서 UI 바꿈
                                Log.d(TAG, "getNewTeam: list.size():" + list.size());
                                for (int index = 0; index < list.size(); index ++){
                                    String memberId = list.get(index);
                                    new GetMemberHandler()
                                            .setIndex(index)
                                            .getUserById(memberId);
                                }
                                break;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            sDatabase.child(TeamNetwork.FIREBASE_TEAMS)
                    .addListenerForSingleValueEvent(snapshotListener);
        }

        //멤버 아이디로 멤버를 가져와서 UI를 바꾸는 Handler
        public class GetMemberHandler extends UserNetwork.GetUserHandler {
            private int mIndex;

            public GetMemberHandler() {
                super();
            }

            //팀에서 멤버의 index와 바꿀 View의 index를 대응시키기 위한 함수
            public GetMemberHandler setIndex(int index) {
                GetMemberHandler getMemberByIdHandler = GetMemberHandler.this;
                getMemberByIdHandler.mIndex = index;
                Log.d(TAG, "setIndex:" + mIndex);
                return getMemberByIdHandler;
            }

            @Override
            public void doWithUserSnapshot(DataSnapshot userSnapshot) {
                GetNewTeamHandler.this.doWithUserSnapshot(mIndex, userSnapshot);
            }
        }
    }
}
