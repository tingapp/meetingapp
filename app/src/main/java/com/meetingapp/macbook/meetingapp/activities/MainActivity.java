package com.meetingapp.macbook.meetingapp.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.fragments.ChoiceFragment;
import com.meetingapp.macbook.meetingapp.fragments.MatchedTeamFragment;
import com.meetingapp.macbook.meetingapp.fragments.ProfileFragment;
import com.meetingapp.macbook.meetingapp.fragments.UnmatchedTeamFragment;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.MainActivityLab;

public class MainActivity extends FragmentActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private final int[] TOOLBAR_UNCHECKED_LIST = new int[] {
            R.mipmap.ic_menu_choice_unchecked,
            R.mipmap.ic_toolbar_matched_team_unchecked,
            R.mipmap.ic_toolbar_unmatched_team_unchecked,
            R.mipmap.ic_toolbar_profile_unchecked};
    private final int[] TOOLBAR_CHECKED_LIST = new int[] {
            R.mipmap.ic_toolbar_choice_checked,
            R.mipmap.ic_menu_matched_team_checked,
            R.mipmap.ic_menu_unmatched_team_checked,
            R.mipmap.ic_menu_profile_checked};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivityLab.getInstance(this);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        final ViewPager mainViewPager = (ViewPager) findViewById(R.id.activity_main_container);
        ImageView ToolbarChoice = (ImageView) findViewById(R.id.activity_main_choice_image);
        ImageView ToolbarMatchedTeam = (ImageView) findViewById(R.id.activity_main_matchedteam_image);
        ImageView ToolbarUnmatchedTeam = (ImageView) findViewById(R.id.activity_main_unmatchedteam_image);
        ImageView ToolbarProfile = (ImageView) findViewById(R.id.activity_main_profile_image);
        final ImageView[] toolbarImageList = new ImageView[] {ToolbarChoice, ToolbarMatchedTeam, ToolbarUnmatchedTeam, ToolbarProfile};

        mainViewPager.setAdapter(new FragmentPagerAdapter(fm) {
            @Override
            public Fragment getItem(int position) {
                return MainActivityLab.getInstance().getFragment(position);
            }

            @Override
            public int getCount() {
                return 4;
            }
        });
        mainViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mPrevPosition = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                toolbarImageList[mPrevPosition].setImageResource(TOOLBAR_UNCHECKED_LIST[mPrevPosition]);
                toolbarImageList[position].setImageResource(TOOLBAR_CHECKED_LIST[position]);
                mPrevPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        LinearLayout choiceButtonView = (LinearLayout) findViewById(R.id.activity_main_choice_button_view);
        LinearLayout matchedTeamButtonView = (LinearLayout) findViewById(R.id.activity_main_matchedteam_button_view);
        LinearLayout unmatchedTeamButtonView = (LinearLayout) findViewById(R.id.activity_main_unmatchedteam_button_view);
        LinearLayout profileButtonView = (LinearLayout)findViewById(R.id.activity_main_profile_button_view);

        choiceButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainViewPager.setCurrentItem(0,true);
            }
        });
        matchedTeamButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainViewPager.setCurrentItem(1,true);
            }
        });
        unmatchedTeamButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainViewPager.setCurrentItem(2,true);
            }
        });
        profileButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainViewPager.setCurrentItem(3,true);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }
}
