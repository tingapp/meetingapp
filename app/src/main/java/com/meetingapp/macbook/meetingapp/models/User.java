package com.meetingapp.macbook.meetingapp.models;

import java.util.List;
import java.util.Map;

/**
 * Created by macbook on 16. 8. 13..
 */
public class User {
    private String mNickname;
    private String mUsername;
    private String mAge;
    private String mUniversity;
    private Map<String, String> mUnmatchedTeamIds;
    private Map<String, String> likeTeams;

    public User() {
    }

    public User(String nickname, String username, String age, String university) {
        mNickname = nickname;
        mUsername = username;
        mAge = age;
        mUniversity = university;
    }

    public String getNickname() {
        return mNickname;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getAge() {
        return mAge;
    }

    public String getUniversity() {
        return mUniversity;
    }

    public Map<String, String> getUnmatchedTeamIds() {
        return mUnmatchedTeamIds;
    }

    public Map<String, String> getLikeTeams() {
        return likeTeams;
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public void setUniversity(String university) {
        mUniversity = university;
    }

    public void setUnmatchedTeamIds(Map<String, String> unmatchedTeamIds) {
        mUnmatchedTeamIds = unmatchedTeamIds;
    }

    public void setLikeTeams(Map<String, String> likeTeams) {
        this.likeTeams = likeTeams;
    }
}
