package com.meetingapp.macbook.meetingapp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.meetingapp.macbook.meetingapp.R;

/**
 * Created by macbook on 16. 8. 4..
 */
public class UnmatchedTeamDetailActivity extends AppCompatActivity {
    private static final String TAG = UnmatchedTeamDetailActivity.class.getSimpleName();

    private LinearLayout user3View;
    private LinearLayout user4View;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unmatched_team_detail);

        final ImageView user1Image = (ImageView) findViewById(R.id.activity_unmatched_team_detail_user01_image);
        final ImageView user2Image = (ImageView) findViewById(R.id.activity_unmatched_team_detail_user02_image);
        final ImageView user3Image = (ImageView) findViewById(R.id.activity_unmatched_team_detail_user03_image);
        final ImageView user4Image = (ImageView) findViewById(R.id.activity_unmatched_team_detail_user04_image);

        final TextView user1Name = (TextView) findViewById(R.id.activity_unmatched_team_detail_user01_name);
        final TextView user2Name = (TextView) findViewById(R.id.activity_unmatched_team_detail_user02_name);
        final TextView user3Name = (TextView) findViewById(R.id.activity_unmatched_team_detail_user03_name);
        final TextView user4Name = (TextView) findViewById(R.id.activity_unmatched_team_detail_user04_name);

        user3View = (LinearLayout) findViewById(R.id.activity_unmatched_team_detail_user03);
        user4View = (LinearLayout) findViewById(R.id.activity_unmatched_team_detail_user04);



        //USER 명수에 따라 프로필 보여주는 경우 다르게 설정
        if (user3Name != null) {
            if (user4Name != null) {
                user3View.setVisibility(View.VISIBLE);
                user4View.setVisibility(View.VISIBLE);
            } else {
                user3View.setVisibility(View.VISIBLE);
            }
        }
    }
}
