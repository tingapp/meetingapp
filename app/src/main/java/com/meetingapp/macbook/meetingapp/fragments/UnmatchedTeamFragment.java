package com.meetingapp.macbook.meetingapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.activities.CreateTeamActivity;
import com.meetingapp.macbook.meetingapp.activities.UnmatchedTeamDetailActivity;
import com.meetingapp.macbook.meetingapp.models.User;
import com.meetingapp.macbook.meetingapp.network.TeamNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 16. 8. 15..
 */
public class UnmatchedTeamFragment extends Fragment {
    private static final String TAG = UnmatchedTeamFragment.class.getSimpleName();

    private LinearLayout mTeamListView;
    private List<String> mMemberNameList = new ArrayList<>();
    private List<String> mMemberIdList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unmatchedteam, container, false);

        Log.d(TAG, "onCreateView");

        View createTeam = view.findViewById(R.id.fragment_unmatchedteam_create_team);
        createTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CreateTeamActivity.class);
                startActivity(intent);
            }
        });

        mTeamListView = (LinearLayout) view.findViewById(R.id.fragment_unmatchedteam_team_list);

        new UnmatchedTeamsHandler().getTeams(UnmatchedTeamsHandler.STATUS_UNMATCHED, UserLab.getInstance().getUserId());

        return view;
    }

    private class UnmatchedTeamsHandler extends UserNetwork.GetTeamsHandler {
        public UnmatchedTeamsHandler() {
            super();
        }

        @Override
        public void doWithTeamSnapshot(DataSnapshot teamSnapshot) {
            ItemHolder itemHolder = new ItemHolder(teamSnapshot);
            mTeamListView.addView(itemHolder.getView());
        }
    }

    private class ItemHolder{
        private final LinearLayout mItemUnmatchedTeamView;
        private final TextView mMembersUsernameTextView;

        public ItemHolder(DataSnapshot teamSnapshot) {
            mItemUnmatchedTeamView = (LinearLayout) View.inflate(getContext(), R.layout.item_unmatchedteam, null);
            mMembersUsernameTextView = (TextView) mItemUnmatchedTeamView.findViewById(R.id.item_unmatchedteam_members_username);
            Log.d(TAG, "new ItemHolder is created, teamId:" + teamSnapshot.getKey());
            new SetItemHolderHandler().getMember(teamSnapshot.getKey());

            mItemUnmatchedTeamView.setOnClickListener(new View.OnClickListener() {
               @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), UnmatchedTeamDetailActivity.class);
                    Log.d(TAG, "onCreateDetailActivity");
                    startActivity(intent);
               }
            });
        }

        public View getView() {
            return mItemUnmatchedTeamView;
        }

        private class SetItemHolderHandler extends TeamNetwork.GetTeamHandler {
            public SetItemHolderHandler() {super();}

            @Override
            public void doWithMemberSnapshot(DataSnapshot memberSnapshot) {
                ItemHolder.this.mMembersUsernameTextView.append(memberSnapshot.getValue(User.class).getUsername() + " ");
            }
        }
    }
}
