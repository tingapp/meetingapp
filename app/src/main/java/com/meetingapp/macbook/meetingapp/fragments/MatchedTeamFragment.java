package com.meetingapp.macbook.meetingapp.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.activities.ChattingActivity;
import com.meetingapp.macbook.meetingapp.models.User;
import com.meetingapp.macbook.meetingapp.network.TeamNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

/**
 * Created by macbook on 16. 8. 15..
 */
public class MatchedTeamFragment extends Fragment {
    private static final String TAG = MatchedTeamFragment.class.getSimpleName();
    private LinearLayout mTeamListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matchedteam, container, false);
        Log.d(TAG, "onCreateView");

        mTeamListView = (LinearLayout) view.findViewById(R.id.fragment_matchedteam_team_list);

        new MatchedTeamsHandler().getTeams(MatchedTeamsHandler.STATUS_MATCHED, UserLab.getInstance().getUserId());

        return view;
    }

    private class MatchedTeamsHandler extends UserNetwork.GetTeamsHandler {
        public MatchedTeamsHandler() {
            super();
        }

        @Override
        public void doWithTeamSnapshot(DataSnapshot teamSnapshot) {
            ItemHolder itemHolder = new ItemHolder(teamSnapshot);
            mTeamListView.addView(itemHolder.getView());
        }
    }

    private class ItemHolder{
        private final LinearLayout mItemUnmatchedTeamView;
        private final TextView mMembersUsernameTextView;

        public ItemHolder(final DataSnapshot teamSnapshot) {
            mItemUnmatchedTeamView = (LinearLayout) View.inflate(getContext(), R.layout.item_matchedteam, null);
            mItemUnmatchedTeamView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ChattingActivity.class);
                    intent.putExtra(ChattingActivity.CHATTING_ID, teamSnapshot.child(TeamNetwork.FIREBASE_STATUS).getValue(String.class));
                    startActivity(intent);
                }
            });//레이아웃 클릭하면 채팅창으로 넘어감, intent로 teamId값 넘김

            mMembersUsernameTextView = (TextView) mItemUnmatchedTeamView.findViewById(R.id.item_matchedteam_member_usernames);
            Log.d(TAG, "new ItemHolder is created, teamId:" + teamSnapshot.getKey());

            new SetItemHolderHandler().getMember(teamSnapshot.getKey());
        }

        public View getView() {
            return mItemUnmatchedTeamView;
        }

        private class SetItemHolderHandler extends TeamNetwork.GetTeamHandler {
            private SetItemHolderHandler() {}

            @Override
            public void doWithMemberSnapshot(final DataSnapshot memberSnapshot) {
                ItemHolder.this.mMembersUsernameTextView.append(memberSnapshot.getValue(User.class).getUsername() + " ");

            }
        }
    }
}
