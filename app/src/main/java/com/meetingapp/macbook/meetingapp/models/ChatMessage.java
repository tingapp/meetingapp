package com.meetingapp.macbook.meetingapp.models;

/**
 * Created by macbook on 16. 8. 31..
 */
public class ChatMessage {
    private String mText;
    private String mUserId;

    public ChatMessage() {

    }

    public ChatMessage(String text, String userId) {
        mText = text;
        mUserId = userId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }
}
