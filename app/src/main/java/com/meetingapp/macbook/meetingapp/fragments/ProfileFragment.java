package com.meetingapp.macbook.meetingapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.activities.ProfileEditActivity;
import com.meetingapp.macbook.meetingapp.activities.ProfileSettingsActivity;
import com.meetingapp.macbook.meetingapp.models.User;
import com.meetingapp.macbook.meetingapp.network.StorageNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

/**
 * Created by macbook on 16. 8. 15..
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = ProfileFragment.class.getSimpleName();

    public static final String PROFILE_NICKNAME = "nickname";
    public static final String PROFILE_USERNAME = "username";
    public static final String PROFILE_AGE = "age";
    public static final String PROFILE_UNIVERSITY = "university";

    private TextView mUserNicknameTextView;
    private TextView mUsernameTextView;
    private TextView mAgeTextView;
    private TextView mUniversityTextView;
    private User mUser;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Log.d(TAG, "onCreateView");

        mUserNicknameTextView = (TextView) view.findViewById(R.id.fragment_profile_nickname);
        mUsernameTextView = (TextView) view.findViewById(R.id.fragment_profile_username);
        mAgeTextView = (TextView) view.findViewById(R.id.fragment_profile_age);
        mUniversityTextView = (TextView) view.findViewById(R.id.fragment_profile_university);
        ImageView editProfileButtonImageView = (ImageView)view.findViewById(R.id.fragment_profile_edit_profile);
        View editSettingsButton = view.findViewById(R.id.fragment_profile_edit_settings);

        String userId = UserLab.getInstance().getUserId();
        new ProfileHandler().getUserById(userId);
        StorageNetwork.downloadImage(userId, "profile1", editProfileButtonImageView);

        editProfileButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                intent.putExtra(PROFILE_NICKNAME, mUser.getNickname());
                intent.putExtra(PROFILE_USERNAME, mUser.getUsername());
                intent.putExtra(PROFILE_AGE, mUser.getAge());
                intent.putExtra(PROFILE_UNIVERSITY, mUser.getUniversity());
                startActivity(intent);
            }
        });

        editSettingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileSettingsActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }

    private class ProfileHandler extends UserNetwork.GetUserHandler {
        private ProfileHandler() {
            super();
        }

        @Override
        public void doWithUserSnapshot(DataSnapshot userSnapshot) {
            mUser = userSnapshot.getValue(User.class);
            mUserNicknameTextView.setText(mUser.getNickname());
            mUsernameTextView.setText(mUser.getUsername());
            mAgeTextView.setText(mUser.getAge());
            mUniversityTextView.setText(mUser.getUniversity());
        }
    }
}
