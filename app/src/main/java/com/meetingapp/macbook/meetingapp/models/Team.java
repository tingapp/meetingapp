package com.meetingapp.macbook.meetingapp.models;

import java.util.List;

/**
 * Created by macbook on 16. 8. 16..
 */
public class Team {
    private String mIntroduction;
    private String mStatus;
    private List<String> mMemberIds;

    public Team(List<String> memberIds, String status, String introduction) {
        mMemberIds = memberIds;
        mStatus = status;
        mIntroduction = introduction;
    }

    public Team() {
    }

    public List<String> getMemberIds() {
        return mMemberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        mMemberIds = memberIds;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

    public void setIntroduction(String introduction) {
        mIntroduction = introduction;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
