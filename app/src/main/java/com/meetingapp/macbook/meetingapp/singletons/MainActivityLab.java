package com.meetingapp.macbook.meetingapp.singletons;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.meetingapp.macbook.meetingapp.activities.MainActivity;
import com.meetingapp.macbook.meetingapp.fragments.ChoiceFragment;
import com.meetingapp.macbook.meetingapp.fragments.MatchedTeamFragment;
import com.meetingapp.macbook.meetingapp.fragments.ProfileFragment;
import com.meetingapp.macbook.meetingapp.fragments.UnmatchedTeamFragment;

/**
 * Created by macbook on 16. 8. 30..
 */
public class MainActivityLab {
    private static MainActivityLab sMainActivityLab;
    private ChoiceFragment mChoiceFragment;
    private MatchedTeamFragment mMatchedTeamFragment;
    private UnmatchedTeamFragment mUnmatchedTeamFragment;
    private ProfileFragment mProfileFragment;
    private MainActivity mMainActivity;
    private Fragment[] mFragmentList;

    private MainActivityLab(MainActivity mainActivity) {
        mMainActivity = mainActivity;
        mChoiceFragment = new ChoiceFragment();
        mMatchedTeamFragment = new MatchedTeamFragment();
        mUnmatchedTeamFragment = new UnmatchedTeamFragment();
        mProfileFragment = new ProfileFragment();
        mFragmentList = new Fragment[] {mChoiceFragment, mMatchedTeamFragment, mUnmatchedTeamFragment, mProfileFragment};
    }

    public static MainActivityLab getInstance() {
        return sMainActivityLab;
    }

    public static MainActivityLab getInstance(MainActivity mainActivity) {
        if (sMainActivityLab == null) {
            sMainActivityLab = new MainActivityLab(mainActivity);
        }
        return sMainActivityLab;
    }

    public void finishActivity() {
        mMainActivity.finish();
    }

    public Fragment getFragment(int position) {
        return mFragmentList[position];
    }
}
