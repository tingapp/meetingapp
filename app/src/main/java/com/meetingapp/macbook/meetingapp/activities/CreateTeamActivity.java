package com.meetingapp.macbook.meetingapp.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.models.User;
import com.meetingapp.macbook.meetingapp.network.TeamNetwork;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macbook on 16. 8. 16..
 */
public class CreateTeamActivity extends AppCompatActivity{

    private static final String TAG = CreateTeamActivity.class.getSimpleName();

    private EditText mNicknameForFindFriendEditText;
    private TextView mAddMemberTextView;
    private LinearLayout mAddMemberView;
    private LinearLayout mAddedFriendsView;

    private String mFriendId;
    private String mNickname;
    private User mFriend;
    private List<String> mAddedFriendIdList = new ArrayList<>();
    private List<String> mAddedNicknameList = new ArrayList<>();
    private List<User> mAddedFriendList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        mNicknameForFindFriendEditText = (EditText) findViewById(R.id.activity_create_team_nickname_for_finding_friend);
        mAddMemberTextView = (TextView) findViewById(R.id.activity_create_team_add_member_text_view);
        final ImageView SearchIcon = (ImageView) findViewById(R.id.activity_create_team_nickname_search_icon);
        final Button findFriendButton = (Button) findViewById(R.id.activity_create_team_find_friend);

        new GetMyNicknameHandler().getUserById(UserLab.getInstance().getUserId());


        mNicknameForFindFriendEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mNicknameForFindFriendEditText.getText().toString().isEmpty()) {
                    findFriendButton.setVisibility(View.GONE);
                    SearchIcon.setVisibility(View.VISIBLE);
                } else {
                    findFriendButton.setVisibility(View.VISIBLE);
                    SearchIcon.setVisibility(View.GONE);
                }
            }
        });

        findFriendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nickname = mNicknameForFindFriendEditText.getText().toString();
                Log.d(TAG, "onclick:nickname:" + nickname);
                if (mNicknameForFindFriendEditText.getText().toString().isEmpty()) {
                    mNicknameForFindFriendEditText.setError("Please type your friend's ID");
                } else if (mAddedNicknameList.contains(nickname)) {
                    Toast.makeText(CreateTeamActivity.this,
                            "you already found the user, please type the other one",
                            Toast.LENGTH_SHORT
                    ).show();
                } else if(nickname.equals(mNickname)) {
                    Toast.makeText(CreateTeamActivity.this,
                            "you can't add your own id, please type your friends'",
                            Toast.LENGTH_SHORT).show();
                } else {
                    new FindFriendHandler().findUser(FindFriendHandler.CLUETYPE_NICKNAME, nickname);
                }
            }
        });

        mAddMemberView = (LinearLayout) findViewById(R.id.activity_create_team_add_member);
        mAddedFriendsView = (LinearLayout)findViewById(R.id.activity_create_team_added_friends);
        mAddedFriendIdList.add(UserLab.getInstance().getUserId());

        ImageView addNewMemberView = (ImageView) findViewById(R.id.activity_create_team_add_member_button);
        addNewMemberView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mAddedFriendIdList.size() > 3) {
                    Toast.makeText(CreateTeamActivity.this,
                            "You can't add more people!!", Toast.LENGTH_SHORT).show();
                } else {
                        mAddedFriendIdList.add(mFriendId);
                        mAddedNicknameList.add(mFriend.getNickname());
                        mAddedFriendList.add(mFriend);
                        mAddedFriendsView.addView(new ItemHolder(mFriendId, mFriend).getView());
                        mAddMemberView.setVisibility(View.GONE);
                        Log.d(TAG, "addNewMemberButton" + mAddedNicknameList + mAddedFriendList);
                    }
                }
        });

        final TextView introductionEditText = (TextView) findViewById(R.id.activity_create_team_introduction);

        final String unClickableColor = "#FF888889";
        final TextView createTeamButton = (TextView) findViewById(R.id.activity_create_team_create_team);
        createTeamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAddedFriendIdList.size() < 2) {
                    createTeamButton.setClickable(false);
                    createTeamButton.setTextColor(Color.parseColor(unClickableColor));
//                    Toast.makeText(CreateTeamActivity.this,
//                            "You should add more people!!", Toast.LENGTH_SHORT).show();
                } else {
                    String introduction = introductionEditText.getText().toString();
                    TeamNetwork.setTeam(mAddedFriendIdList, introduction);
                    Toast.makeText(CreateTeamActivity.this, "the team is successfully created", Toast.LENGTH_SHORT).show();

                    //UnmatchedTeamFragment에 추가된 팀을  UI에 보여줘야함

                    CreateTeamActivity.this.finish();

                }
            }
        });
    }

    private class FindFriendHandler extends UserNetwork.FindUserHandler {
        private FindFriendHandler() {
            super();
        }

        @Override
        public void doWithUserSnapshot(DataSnapshot userSnapshot) {
            mFriendId = userSnapshot.getKey();
            mFriend = userSnapshot.getValue(User.class);
            mAddMemberTextView.setText(mFriend.getUsername());
            mNicknameForFindFriendEditText.setText("");
            mAddMemberView.setVisibility(View.VISIBLE);
        }

        @Override
        public void doWhenfriendIsNotFound() {
            Toast.makeText(CreateTeamActivity.this,
                    "there is no user with this ID, please type the other one",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    private class ItemHolder {
        private final LinearLayout mItemCreateTeamView;

        public ItemHolder(String friendId, User friend) {
            Log.d(TAG, "ItemHolder is created");
            final String mItemFriendId = friendId;
            final User mItemFriend = friend;

            mItemCreateTeamView = (LinearLayout) View.inflate(getApplicationContext(), R.layout.item_create_team, null);
            TextView mItemFriendIdAndNameTextView = (TextView) mItemCreateTeamView.findViewById(R.id.item_create_team_friend_id_and_name);
            ImageView mItemWithdrawView = (ImageView) mItemCreateTeamView.findViewById(R.id.item_create_team_withdraw_button);

            mItemFriendIdAndNameTextView.setText(mItemFriend.getNickname());
            mItemWithdrawView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAddedFriendIdList.remove(mItemFriendId);
                    mAddedNicknameList.remove(mItemFriend.getNickname());
                    mAddedFriendList.remove(mItemFriend);
                    mAddedFriendsView.removeView(mItemCreateTeamView);
                    Log.d(TAG, "ItemHolder" + mAddedNicknameList + mAddedFriendList);
                }
            });
        }

        public View getView() {
            return mItemCreateTeamView;
        }
    }

    private class GetMyNicknameHandler extends UserNetwork.GetUserHandler {
        private GetMyNicknameHandler() {
            super();
        }

        @Override
        public void doWithUserSnapshot(DataSnapshot userSnapshot) {
            mNickname = userSnapshot.getValue(User.class).getNickname();
        }
    }
}
