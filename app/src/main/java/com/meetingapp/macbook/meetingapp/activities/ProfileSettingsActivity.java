package com.meetingapp.macbook.meetingapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.meetingapp.macbook.meetingapp.R;
import com.meetingapp.macbook.meetingapp.network.UserNetwork;
import com.meetingapp.macbook.meetingapp.singletons.MainActivityLab;
import com.meetingapp.macbook.meetingapp.singletons.UserLab;

/**
 * Created by macbook on 16. 8. 26..
 */
public class ProfileSettingsActivity extends AppCompatActivity {

    private static final String TAG = ProfileSettingsActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);

        View logoutButtonView = findViewById(R.id.activity_profile_settings_btn_logout);
        logoutButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onAuthStateChanged:signed_out:");

                logout();
                Toast.makeText(ProfileSettingsActivity.this, "Succesfully logged out",Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(ProfileSettingsActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ImageView backButtonImageView = (ImageView) findViewById(R.id.activity_profile_settings_back);
        backButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        View btnRemoveUser = findViewById(R.id.activity_profile_settings_btn_remove);
        btnRemoveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 회원과 관련된 Child 다 날려버리는 코드 작성
                // Dialog으로 구현하도록 수정
                Log.d(TAG, "onAuthStateChanged:removeUser");

                deleteUser();
            }
        });
    }

    private void logout() {
        Log.d(TAG, "logout");
        AccessToken.setCurrentAccessToken(null); //페이스북 로그아웃
        FirebaseAuth.getInstance().signOut(); //Firebase 로그아웃
        MainActivityLab.getInstance().finishActivity(); // MainActivity finish()
        UserLab.getInstance().initiateUserId();//싱글톤에서 아이디 지우기
    }

    private void deleteUser() {
        Log.d(TAG, "deleteUser");

        FirebaseAuth.getInstance()
                .getCurrentUser()
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "deleteUser:onComplete");

                            AccessToken.setCurrentAccessToken(null); //페이스북 로그아웃
                            UserNetwork.sDatabase.child(UserNetwork.FIREBASE_USERS).child(UserLab.getInstance().getUserId()).removeValue(); //디비에서 유저정보 지우기
                            MainActivityLab.getInstance().finishActivity(); // MainActivity finish()
                            UserLab.getInstance().initiateUserId();//싱글톤에서 아이디 지우기

                            Toast.makeText(ProfileSettingsActivity.this, "Your account is successfully deleted", Toast.LENGTH_SHORT);

                            Intent intent = new Intent(ProfileSettingsActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish(); // ProfileSettingsActivity finish()
                        }
                    }
                });
    }
}
