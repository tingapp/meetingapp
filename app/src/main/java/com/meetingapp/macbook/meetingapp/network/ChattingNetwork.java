package com.meetingapp.macbook.meetingapp.network;

import com.meetingapp.macbook.meetingapp.models.ChatMessage;

/**
 * Created by macbook on 16. 8. 31..
 */
public class ChattingNetwork extends DatabaseNetwork {
    public static final String FIREBASE_CHATTING = "chatting";
    public static final String FIREBASE_TEAMIDS = "teamIds";
    private static final String FIREBASE_MESSAGES = "messages";

    public static void sendMessage(String chattingId, ChatMessage chatMessage) {
        sDatabase.child(FIREBASE_CHATTING)
                .child(chattingId)
                .child(FIREBASE_MESSAGES)
                .push().setValue(chatMessage);
    }
}
